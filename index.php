<?php
/**
 * 拾光博客模板
 * 
 * @package 拾光博客 
 * @author 拾光博客(黄康权)
 * @version 1.0
 * @link http://www.hiai.top/
 */

if (!defined('__TYPECHO_ROOT_DIR__')) exit;
 $this->need('header.php');
 ?>
 <div class="w1000">
	<div class="blog_content" id="blog_content">
		<ul>
			<?php if($this->have()):?>
			<?php while($this->next()): ?>
			<li class="wow zoomIn animated" style="visibility: visible; animation-name: zoomIn;">
				<a href="<?php $this->permalink() ?>" class="article" itemprop="name headline">
					<div class="blog_image">   
						<img src="<?php echo showThumb($this,null,true); ?>" data-src="<?php echo showThumb($this,null,true); ?>" class="thumb">
					</div>
					<h3><?php $this->title() ?></h3>
					<div>
						<div class="article_content">
							<p><?php $this->excerpt(111, '...'); ?></p>
						</div>
						<div class="article_info">
							<span class="blog_heart"><i class="fa fa fa-user"></i><?php $this->author(); ?></span>
							<span><i class="fa fa-eye"></i><?php get_post_view($this) ?></span>
							<span class="blog_comment"><i class="fa fa-commenting-o"></i><?php $this->commentsNum('0', '1', '%d'); ?></span>
							<span class="blog_date"><i class="fa fa-clock-o"></i><?php $this->date('Y-m-d'); ?></span>
						</div>
					</div>
				</a>
				<div class="article_classify" name="6"><?php $this->category(','); ?></div>
				<a href="<?php $this->permalink() ?>" target="_0" class="read_more">阅读全文</a>
			</li>
			<?php endwhile; ?>
			<?php endif; ?>		
		</ul>
		
		<?php $this->pageNav('&laquo; 前一页', '后一页 &raquo;'); ?>
	</div>

	<?php $this->need('sidebar.php'); ?>
	
</div>

<?php $this->need('footer.php'); ?>