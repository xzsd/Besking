<?php
/**
 * 页面归档
 *
 * @package custom
 */
 if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<?php $this->need('header.php'); ?>

<div class="w700">
	<div id="detail" class="wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
	<h3 class="article_title"><?php $this->title() ?></h3>
	<div class="detail_info"></div>
	<?php $this->widget('Widget_Contents_Post_Recent', 'pageSize=10000')->to($archives);
	$year=0; $mon=0; $i=0; $j=0;
	$output = '<article class="archives">';
	while($archives->next()):
	$year_tmp = date('Y',$archives->created);
	$mon_tmp = date('m',$archives->created);
	$y=$year; $m=$mon;
	if ($mon != $mon_tmp && $mon > 0) $output .= '</ul></div>';
	if ($year != $year_tmp && $year > 0) $output .= '';
	if ($year != $year_tmp) {
	$year = $year_tmp;
	}
	if ($mon != $mon_tmp) {
	$mon = $mon_tmp;
	$output .= '<div class="item"><h3>'. $year . '年' . $mon .' 月</h3><ul class="archives-list">'; //输出月份
	}
	$output .= '<li style="padding-top: 8px;"><time>'.date('d日: ',$archives->created).'</time><a href="'.$archives->permalink .'">'. $archives->title .'</a> <span class="text-muted">'. $archives->commentsNum.'评论</span></li>'; //输出文章日期和标题
	endwhile;
	$output .= '</ul></div></article>';
	echo $output;
	?>

	</div>
</div>

<?php $this->need('footer.php'); ?>