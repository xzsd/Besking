<?php
/**
 * 关于我
 *
 * @package custom
 */
 if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<?php $this->need('header.php'); ?>

<div class="w1000">
	<div class="about_left">
		<div>
			<h3>About Me</h3>
			<p><?php $this->content(); ?></p>
		</div>
		<div>
			<h3>About Rights</h3>
			<p>
				本站由<a href="/"><?php $this->options->title() ?> </a>博客创作，采用
				<a href="" target="_blank">知识共享署名4</a>国际许可协议进行许可
			</p>
			<p>本站文章除注明转载/出处外，均为本站原创或翻译，转载前请务必署名</p>
			<p>只要您在使用时注明出处，就可以对本站所有原创内容进行转载、节选、二次创作，但是您不得对其用于商业目的
			</p>
		</div>
	</div>
	<div class="about_right">
		<div class="about_me">
			<h2>——About Me——</h2>
			<?php if (!empty($this->options->aboutabotUrl)): ?>
				<img src="<?php $this->options->aboutabotUrl(); ?>" alt="关于我" />
			<?php else: ?>
				<img src="https://www.hiai.top/yuan/logo-1.png"alt="关于我" height="100px">
			<?php endif; ?>
			<div>
				<p>网名：<?php $this->author(); ?></p>
				<p>个性签名：
					<?php if (!empty($this->options->autographUrl)): ?>
						<?php $this->options->autographUrl(); ?>
					<?php else: ?>
						知识改变命运-拾光博客
					<?php endif; ?>	
				</p>
			</div>
		</div>
		<div class="weChat">
			<h2>
				——WeChat——
			</h2>
			<?php if (!empty($this->options->aboutweixinUrl)): ?>
				<img src="<?php $this->options->aboutweixinUrl(); ?>" alt="关于我" />
			<?php else: ?>
				<img src="https://www.hiai.top/yuan/weixin.jpg"alt="关于我" width="250px" height="250px">
			<?php endif; ?>
		</div>
	</div>
</div>

<?php $this->need('footer.php'); ?>