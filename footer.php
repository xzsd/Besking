<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>

<!--遮罩层开始-->
<div id="overLay" hidden="hidden"></div>
<div>
	<span class="wow fadeInRight left1">&lt;</span>
	<span class="wow fadeInRight left2">&lt;</span>
	<span class="top1 wow fadeInLeft"><i class="fa fa-search"></i></span>
</div>
<!--遮罩层结束-->

<!--底部开始-->
<div id="footer"> 
    <?php if (!empty($this->options->gooterGithub)): ?>
        <a href="<?php $this->options->gooterGithub(); ?>" target="_blank"><i class="fa fa-github"></i></a>
    <?php else: ?>
        <a href="https://gitee.com/hkq15" target="_blank"><i class="fa fa-github"></i></a>
    <?php endif; ?>
    <a href="https://wpa.qq.com/msgrd?v=3&uin=<?php $this->options->gooterQQ(); ?>&site=qq&menu=yes" target="_blank"><i class="fa fa-qq"></i></a>
    <a href="mailto:<?php $this->options->gooterEmail(); ?>" target="_blank"><i class="fa fa-envelope-o"></i></a>
    <a href="<?php $this->options->gooterGitee(); ?>" target="_blank"><i class="fa fa-google-plus"></i></a>
</div>
<!--请保留版权，尊重作者。-->
<div id="fooCopyright">
 <p>Copyright &copy; 2017-<?php echo date("Y"); ?> 
    <a href="<?php $this->options->siteUrl(); ?>"><?php $this->options->title() ?> </a>
    Powered by <a href="http://typecho.org/" target="_blank" >Typecho</a>
    Theme By <a href="https://www.hiai.top/">hiai</a>
    </p>
</div>
<!--底部结束-->


<!--一键回到顶部-->
<div id="fixBars">
	<ul class="fix_bar">
        <li id="toTop" hidden="hidden" class="wow fadeInRight"><i class="fa fa-arrow-up"></i>
        </li>
        <li id="feedBack"><i class="fa fa-heart"></i>
        </li>
    </ul>
</div>

<?php $this->footer(); ?>
<script src="<?php $this->options->themeUrl('js/jquery.min.js'); ?>"></script>
<script src="<?php $this->options->themeUrl('js/libs/wow.min.js'); ?>"></script>
<script src="https://lib.baomitu.com/sweetalert/2.1.0/sweetalert.min.js"></script>
<script src="<?php $this->options->themeUrl('js/libs/art-template.js'); ?>"></script>
<script src="<?php $this->options->themeUrl('js/base.js'); ?>"></script>
<?php if($this->options->GoogleAnalytics): ?>
<?php $this->options->GoogleAnalytics(); ?>
<?php else: ?>
<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?9686e822163d5ca3861fec88310cbee3";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>
<?php endif; ?>
</body>
</html>